package com.servicios;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitecu/v2")
public class ServiciosController {
    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filtro){
        return ServiciosService.getFiltrados(filtro);
    }

    @PostMapping("/servicios")
    public String setServicio(@RequestBody String newServicio){
        try{
            ServiciosService.insert(newServicio);
            return "OK";
        } catch (Exception ex){
            return ex.getMessage();
        }
    }

    @PutMapping("/servicios")
    public String updServicio(@RequestBody String data){
        try {
            JSONObject obj = new JSONObject(data);// se genera objeto json con el data recibido para el update
            String filtro = obj.getJSONObject("filtro").toString();//accede a las propiedad filtro del json y lo convierte en string
            String updates = obj.getJSONObject("updates").toString();//accede a las propiedad update del json y lo convierte en string
            ServiciosService.update(filtro,updates);
        } catch (Exception ex){
            return ex.getMessage();
        } return "ok";
    }
}
