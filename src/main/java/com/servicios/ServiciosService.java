package com.servicios;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import com.mongodb.client.MongoClient;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServiciosService {
    static MongoCollection<Document> servicios;
    private static MongoCollection<Document> getServiciosColection(){
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017"); //cadena de conexion con la ruta del driver
        MongoClientSettings settings =  MongoClientSettings.builder()
                                                            .applyConnectionString(cs)
                                                            .retryWrites(true)
                                                            .build();//contiene las especificaciones de la conexion para tener un cte mongo

        MongoClient mongo = MongoClients.create(settings); // herramienta con la que interactuaremos con la BD
         MongoDatabase database = mongo.getDatabase("dbprod");
         return database.getCollection("servicios");
    }

   /* public static void insert(String servicio) throws Exception{
        Document doc = Document.parse(servicio); // pasa un string con un json para almacenar en tipo documento bson
        servicios = getServiciosColection();
        servicios.insertOne(doc);

    }
    */


    public static void insert(String strServicios) throws Exception {
        servicios =getServiciosColection(); //accede a la coleccion
        Document doc = Document.parse(strServicios); // convierte el string a documento
        List<Document> lst = doc.getList("servicios", Document.class); // crea arreglo de documentos; el nomnre del campo servicios en forma de arreglo es pedida

        if( lst == null){ // valida si solo trae un elemento
            servicios.insertOne(doc);
        } else{
            servicios.insertMany(lst);
        }
    }

    public static List getall(){
        servicios =getServiciosColection(); //accede a la coleccion
        List lst = new ArrayList(); //preparamos el arreglo
        servicios.find(); // es el find para iterar
        FindIterable<Document> iterDoc = servicios.find(); // nos devuelve la lista de documentos
        Iterator it = iterDoc.iterator();
        while (it.hasNext()){
            lst.add(it.next());
        }
        return lst;
    }

    public static List getFiltrados(String filtro){
        servicios =getServiciosColection(); //accede a la coleccion
        List lst = new ArrayList(); //preparamos el arreglo
        Document docfiltro = Document.parse(filtro);
        FindIterable<Document> iterDoc = servicios.find(docfiltro); // nos devuelve la lista de documentos
        Iterator it = iterDoc.iterator();
        while (it.hasNext()){
            lst.add(it.next());
        }
        return lst;
    }

    public static void update(String filtro, String updates){
        servicios =getServiciosColection();//se conecta con la BD y busca la bd
        Document docFiltro =  Document.parse(filtro);
        Document doc = Document.parse(updates);
        servicios.updateOne(docFiltro,doc);//asignara los valores que este en la variable doc por medio del docFiltro encontratra a quien actualizra
    }
}
