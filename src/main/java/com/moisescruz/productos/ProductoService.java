package com.moisescruz.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class ProductoService {
    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoModel> findAll(){
        return productoRepository.findAll();//devolvera los objetos segun la estructura del Prouct model
    }

    public Optional<ProductoModel> findById(String ids){
        return productoRepository.findById(ids);
    }

    public ProductoModel save(ProductoModel prod){
        return productoRepository.save(prod);
    }

    public boolean delete(ProductoModel prodDelete){
        try{
            productoRepository.delete(prodDelete);
            return true;
        } catch(Exception ex){
            return false;
        }
    }
}
